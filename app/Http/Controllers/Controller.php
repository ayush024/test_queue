<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessSleep;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getQueuedJobs(){
    	$queued_processes = \DB::table('jobs')->get();

    	foreach ($queued_processes as $q){
    		$q->payload = json_decode($q->payload);
    	}

    	return response()->json($queued_processes);
    }

    public function dispatchSleep(){
    	ProcessSleep::dispatch();

    	return response()->json(['message'=>'Process added to queue']);
    }

    public function getProcessedJobs(){
        $processed_jobs = \DB::table('processed_jobs')->get();

        foreach ($processed_jobs as $p){
            $p->payload = json_decode($p->payload);
        }

        return response()->json($processed_jobs);
    }
}
