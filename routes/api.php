<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('queued_jobs/', [
	'as'	=>	'queued-jobs-get',
	'uses'	=>	'Controller@getQueuedJobs'
]);

Route::get('dispatch_sleep/', [
	'as'	=>	'dispatch-sleep-post',
	'uses'	=>	'Controller@dispatchSleep'
]);

Route::get('processed_jobs/', [
	'as'	=>	'processed-jobs-get',
	'uses'	=>	'Controller@getProcessedJobs'
]);
