<?php

namespace App\Providers;

use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Queue::after(function (JobProcessed $event) {
            \DB::table('processed_jobs')->insert([
               'connection' => $event->connectionName,
               'queue' => $event->job->getQueue(),
               'payload' => json_encode($event->job->payload()),
               'processed_at' => \Carbon\Carbon::Now()
            ]);
        });
    }
}
